﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dilempar : MonoBehaviour {

    public float kecepatan = 10;
    Rigidbody rb;
	
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
		rb.velocity = transform.forward * kecepatan;

        Invoke("hancur", 5f);
    }
	
	void hancur()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.transform.position = new Vector3(338, 4.7f, 915);
        }
    }
}
