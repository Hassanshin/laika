﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lempar : StateMachineBehaviour {

    public GameObject particle;
    public float radius;
    public float power;

    public float jedaSerang = 1.0f;
    public float x;
    protected GameObject clone;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        clone = Instantiate(particle, animator.rootPosition + new Vector3(0, 1, 0), animator.transform.rotation) as GameObject;
        Rigidbody rb = clone.GetComponent<Rigidbody>();
        rb.AddExplosionForce(power, animator.rootPosition, radius, 3.0f);
        x = 0;

        if (x < 4)
        {
            x += Time.deltaTime * 200;
        } else
        {
            
        }
    }

    public void tembak()
    {
        
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    if (x < jedaSerang)
    //    {
    //        x += Time.deltaTime;
    //    }
    //    else
    //    {
    //        clone = Instantiate(particle, animator.rootPosition + new Vector3(0, 1, 0), animator.transform.rotation) as GameObject;
    //        Rigidbody rb = clone.GetComponent<Rigidbody>();
    //        rb.AddExplosionForce(power, animator.rootPosition, radius, 3.0f);

    //        Instantiate(lemparObject, transform.position + new Vector3(0, 1, 0.2f), transform.rotation);
    //        x = 0;
    //    }


    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        x = 0;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
