﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempatTopeng : MonoBehaviour {

    public GameObject TopengDayak;
    public GameObject TopengBali;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == ("Player"))
        {
            other.GetComponent<PlayerControl>().diTempatTopeng = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == ("Player"))
        {
            other.GetComponent<PlayerControl>().diTempatTopeng = !true;
        }
    }
}
