﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class urutan : MonoBehaviour {

    urutan_check kotak;
    float x = 0;

    public ParticleSystem terbatu;
    public Color gantiColor;
    public Image circleFill;

    private void Start()
    {
        kotak = gameObject.transform.GetChild(0).GetComponent<urutan_check>();
        //circleFill.enabled = false;
    }

    private void Update()
    {
        

        //if(x > 0)
        //{
        //    circleFill.gameObject.transform.parent.gameObject.SetActive(true);
        //} else
        //{
        //    circleFill.gameObject.transform.parent.gameObject.SetActive(!true);
        //}
    }

    private void OnTriggerStay(Collider other)
    {
        circleFill.fillAmount = x * 2;
        if (other.tag == "Player" && Input.GetKey(KeyCode.E))
        {
            
            if (x <= 0.5f)
            {
                x += Time.deltaTime;
            } else
            {
                kotak.Rotasi();
                x = 0;
            }
            
        } else
        {
            x = 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            circleFill.gameObject.transform.parent.gameObject.SetActive(true);
            //circleFill.enabled = true;
            //terbatu.startColor = Color;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            circleFill.gameObject.transform.parent.gameObject.SetActive(!true);
            //circleFill.enabled = !true;
            //terbatu.startColor = Color;
        }
    }
}
