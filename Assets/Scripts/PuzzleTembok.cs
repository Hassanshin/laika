﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleTembok : MonoBehaviour {

    [Range(1, 4)]
    public int nomor;

    public GameObject batu;
    public bool memberiBatu;

    GameObject player;
    float charge = 0;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            player = other.gameObject;

            if (player != null)
            {
                if (Input.GetKey(KeyCode.G))
                {
                    memberiBatu = true;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            player = null;
        }
    }

    private void Update()
    {
        if (memberiBatu && batu != null && player != null)
        {
            
            PuzzleKunci kunciScript = batu.GetComponent<PuzzleKunci>();
            PlayerControl playerScript = player.GetComponent<PlayerControl>();

            kunciScript.emak = null;
            kunciScript.emak = player;
            playerScript.yangdibawa = batu;

            Ulang();

            //reset
            
        } 
        
        
    }
    
    void Ulang()
    {
        batu = null;
        memberiBatu = false;
    }
}
