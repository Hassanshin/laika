﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestScript : MonoBehaviour {

    public List<string> textQuest = new List<string>();

    public List<string> QuestProgress = new List<string>();

    public List<string> QuestDone = new List<string>();

    public Text[] viewProgress;

    public string dummyText = "test";

    int questKosong = 0;
    bool done = false;
    int x = 0;

    bool done2 = false;
    int c = 0;
    int target = 0;

    void Start () {
        

        if (PlayerPrefs.GetInt("sav") == -5)
        {
            AddQuest("Cari Peta di dalam kerajaan");
            Debug.Log(PlayerPrefs.GetInt("sav"));
            
        } else
        {
            Debug.Log(" 0");
        }
       
        
    }

    public void AddQuest(string tex)
    {
        x = 0;
        done = false;
        CheckKosong(); //cek ada textnya atau tidak

        //QuestProgress.Add(textQuest[ke]);
        //masukkin quest dari list string
        viewProgress[questKosong].text = tex;
        viewProgress[questKosong].gameObject.SetActive(true);

        //questKosong++;
    }

    public void QuestSelesai(string tex)
    {
        done2 = false;
        c = 0;

        while (done2 == false && c < 7)
        {
            //Debug.Log(c);
            //Debug.Log(viewProgress[c].text + " ====  " + tex);
            if (viewProgress[c].text == tex)
            {
                
                target = c;

                done2 = true;
            }
            else
            {
                c++;
            }
        }

        viewProgress[target].text = dummyText;
        viewProgress[target].gameObject.SetActive(false);


    }

    void CheckQuestSelseai(string tex)
    {
        
    }

    void CheckKosong()
    {
        
        while (done == false)
        {
            
            if(viewProgress[x].text != dummyText)
            {
                x++;
                //Debug.Log("nambah");
            } else
            {
                questKosong = x;
                //Debug.Log("quest kosong = " + questKosong + "dan x = " + x);

                done = true;
            }
        }
        
        
    }

    private void Update()
    {
        
        
    }
}
