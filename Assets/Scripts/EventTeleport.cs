﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventTeleport : MonoBehaviour {

    public bool pencetTombolE;

    public GameObject toolTip;

    public Transform kesini;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == ("Player"))
        {
            
            toolTip.transform.GetChild(0).GetComponent<Text>().text = "Tekan E";
            toolTip.SetActive(true);
            if (pencetTombolE )
            {
                if (Input.GetKey(KeyCode.E))
                {
                    other.transform.position = kesini.transform.position;
                    //toolTip.SetActive(!true);
                }
                
            } else
            {
                other.transform.position = kesini.transform.position;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == ("Player"))
        {
            toolTip.SetActive(!true);
        }
    }
}
