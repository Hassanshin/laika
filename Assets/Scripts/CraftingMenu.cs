﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingMenu : MonoBehaviour {

    [Header("Panel")]
    public GameObject PanelCrafting;
    public Text stats;

    [Header("Link")]
    public TPSCamera kamera;

    public PlayerControl playerMove;
    //public ObjectiveChar player;

    private void Start()
    {
        PanelCrafting.SetActive(!true);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PanelCrafting.SetActive(true);
            Control(true);
        }
    }

    private void Update()
    {
        //stats.text = "Daun Hijau : " + player.daunHijau +
        //             " Daun Hitam : " + player.daunHitam + 
        //             " Daun Merah " + player.daunMerah +
        //             " Makanan " + player.C_Makan +
        //             " Obat " + player.C_Obat;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PanelCrafting.SetActive(!true);
            Control(!true);
        }
    }

    public void Control(bool kontrol)
    {
        kamera.lockCursor = !kontrol;

        kamera.disableControl = kontrol;
        playerMove.disableControl = kontrol;
    }

    public void C_Obat()
    {
        //if (player.daunHitam > 0 && player.daunHijau > 0)
        //{
        //    player.daunHitam -= 1;
        //    player.daunHijau -= 1;

        //    player.C_Obat += 1;
        //}
    }

    public void C_Makanan()
    {
        //if(player.daunHitam > 0 && player.daunMerah > 0)
        //{
        //    player.daunHitam -= 1;
        //    player.daunMerah -= 1;

        //    player.C_Makan += 1;
        //}
    }

}
