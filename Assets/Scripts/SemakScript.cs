﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SemakScript : MonoBehaviour {

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            other.GetComponent<PlayerControl>().disemak = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerControl>().disemak = !true;
        }
    }
}
