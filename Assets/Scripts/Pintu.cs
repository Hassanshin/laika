﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pintu : MonoBehaviour {

    Animator anim;

    //public GameObject toolTip;
    
    public int pintu_Ke;
    public int minimalPintu1 = 3;

    public int stored = 0;
    int c = 0;

    public GameObject[] viewKunci;
    public GameObject tutorUseItems;

    private void Start()
    {
        anim = GetComponentInParent<Animator>();
    }

    private void Update()
    {
        for (int i = 0; i < stored; i++)
        {
            viewKunci[i].SetActive(true);
        }

        if(stored >= 4 && c == 0)
        {
            Debug.Log("terbuka");

            anim.SetBool("keatas", true);
            //toolTip.SetActive(!true);

            tutorUseItems.SetActive(false);
            
            c = 1;
        } 
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //toolTip.SetActive(true);
            ObjectiveChar playerObj = other.GetComponent<ObjectiveChar>();

            other.GetComponent<PlayerControl>().diPintuLabirin = true;

            if (stored < 4)
            {
                tutorUseItems.SetActive(!false);
            }

            switch (pintu_Ke)
            {
                case 1:
                    if (playerObj.keyCount >= minimalPintu1)
                    {
                        Debug.Log("terbuka");

                        anim.SetBool("keatas", true);
                        //toolTip.SetActive(!true);
                        //Destroy(transform.parent.gameObject);
                    }
                    else
                    {
                        Debug.Log("kunci kurang");
                    }
                    break;
                case 2:
                    if (playerObj.komodoCount >= 5)
                    {
                        Debug.Log("terbuka");

                        anim.SetBool("keatas", true);
                        //Destroy(transform.parent.gameObject);
                    }
                    else
                    {
                        Debug.Log("kunci kurang");
                    }
                    break;
                default:
                    break;
            }
        }
            
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            //toolTip.SetActive(!true);
            other.GetComponent<PlayerControl>().diPintuLabirin = !true;

            if (stored < 4)
            {
                tutorUseItems.SetActive(false);
            }
        }
        
    }
}
