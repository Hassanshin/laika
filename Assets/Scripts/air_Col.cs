﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class air_Col : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerControl>().diAir = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerControl>().diAir = !true;
        }
    }
}
