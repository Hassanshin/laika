﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowScript : MonoBehaviour {

    public Transform player;

    public Transform[] quest;

    Vector3 konversi;

    Vector3 konversi2;

    [Range(0,10)]
    public int x = 0;

    void Start ()
    {
		
	}
	
	void LateUpdate ()
    {
        konversi = player.transform.position;
        konversi.y = player.transform.position.y + 2.74f;

        transform.position = konversi;

        if(quest[x] != null)
        {
            konversi2 = quest[x].transform.position;
            konversi2.y = transform.position.y;

            transform.LookAt(konversi2);
        }
    }
}
