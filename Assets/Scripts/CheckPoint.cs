﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

    public Transform[] tempat;

    public GameObject player;
    public GameObject camera1, camera2;
    public GameObject UI;

    TPSCamera camera1script;
    FPSCamera camera2script;

    public void Start()
    {
        camera1script = camera1.GetComponent<TPSCamera>();
        camera2script = camera2.GetComponent<FPSCamera>();
    }

    public void ke(int nomor)
    {
        player.transform.position = tempat[nomor].transform.position;
        player.GetComponent<PlayerControl>().disableControl = false;
        UI.gameObject.SetActive(false);
        camera1script.lockCursor = true;
        camera1script.disableControl = false;

        camera2script.lockCursor = true;
        camera2script.disableControl = false;

    }
}
