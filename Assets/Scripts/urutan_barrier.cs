﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class urutan_barrier : MonoBehaviour {

    public urutan_check[] kotak;

    public bool[] kotakStatus;

    public PlayableDirector pd;
    public GameObject enabledCamera;
    public GameObject disabledCamera;

    public GameObject arrow;
    public GameObject ibuKomodo;

    QuestScript quest;
    int x = 0;

    private void Start()
    {
        quest = GameObject.Find("Quest List").GetComponent<QuestScript>();
    }

    void Update ()
    {
        for (int i = 0; i < kotak.Length; i++)
        {
            kotakStatus[i] = kotak[i].isTrue;
        }

        if (kotakStatus[0] &&
            kotakStatus[2] &&
            kotakStatus[3] &&
            kotakStatus[1])
        {
            enabledCamera.SetActive(true);
            disabledCamera.SetActive(!true);
            pd.Play();

            Invoke("ilang", 1f);

        } else
        {
            gameObject.SetActive(true);
        }
    }

    void ilang()
    {
        gameObject.SetActive(!true);
        if(x == 0)
        {
            quest.QuestSelesai("Buka Barrier");
            quest.QuestSelesai("Selesaikan Puzzle");

            quest.AddQuest("Cari Ibu Komodo");

            x = 1;
        }
        

        arrow.SetActive(false);
        ibuKomodo.SetActive(true);

        Invoke("balik", 3f);
    }

    void balik()
    {
        enabledCamera.SetActive(!true);
        disabledCamera.SetActive(true);
    }
}
