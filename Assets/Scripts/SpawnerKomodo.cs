﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerKomodo : MonoBehaviour {

    public int jumlahKomodo;

    public GameObject KomodoPrefab;
    public Vector3 random;

    float xMin, xMax, zMin, zMax;

    BoxCollider col;

    private void Start()
    {
        col = GetComponent<BoxCollider>();
        

        xMin = transform.position.x - col.size.x / 4f;
        xMax = transform.position.x + col.size.x / 4f;

        zMin = transform.position.z - col.size.z / 4f;
        zMax = transform.position.z + col.size.z / 4f;

        for (int i = 0; i < jumlahKomodo; i++)
        {
            spawn();
        }
    }

    private void Update()
    {
        
        
    }

    void spawn()
    {
        random = transform.position;
        random.x = Random.Range(xMin, xMax);
        random.z = Random.Range(zMin, zMax);

        GameObject childObj = Instantiate(KomodoPrefab, random, Quaternion.identity);
        childObj.transform.parent = gameObject.transform;
    }
}
