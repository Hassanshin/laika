﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePintu : MonoBehaviour {

    public PuzzleTembok[] gembok;

    public bool[] checkSlot;
    public bool semuaSlot;
    public bool[] cocokSlot;
    public bool buka;

    Animator anim;

    void Start () {
        anim = GetComponent<Animator>();
    }
	
	void Update () {


        for (int i = 0; i < gembok.Length; i++)
        {
            if (gembok[i].batu != null)
            {
                checkSlot[i] = true;
            }
            else
            {
                checkSlot[i] = !true;
            }
        }

        if (checkSlot[0] && checkSlot[1] && checkSlot[2] && checkSlot[3])
        {
            semuaSlot = true;
        } else
        {
            semuaSlot = !true;
        }

        if (semuaSlot)
        {
            for (int i = 0; i < gembok.Length; i++)
            {
                if (gembok[i].nomor == gembok[i].batu.GetComponent<PuzzleKunci>().nomor)
                {
                    cocokSlot[i] = true;
                    Debug.Log("Cocok Kunci ke-" + i);
                }
                else
                {
                    //cocokSlot[i] = !true;
                    Debug.Log("Tidak Cocok Kunci ke-" + i);
                }
            }
        }

        if (cocokSlot[0] && cocokSlot[1] && cocokSlot[2] && cocokSlot[3])
        {
            buka = true;
        }
        else
        {
            buka = !true;
        }

        if (buka)
        {
            Destroy(gameObject);
            //transform.position = transform.position + new Vector3(0, 4, 0);
        }
    }
}
