﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingSpawner : MonoBehaviour {

    public GameObject KomodoPrefab;
    public Vector3 random;

    float xMin, xMax, zMin, zMax;

    BoxCollider col;

    private void Start()
    {
        col = GetComponent<BoxCollider>();


        xMin = transform.position.x - col.size.x / 4f;
        xMax = transform.position.x + col.size.x / 4f;

        zMin = transform.position.z - col.size.z / 4f;
        zMax = transform.position.z + col.size.z / 4f;

        for (int i = 0; i < 5; i++)
        {
            spawn();
        }
    }

    void spawn()
    {
        random = transform.position;
        random.x = Random.Range(xMin, xMax);
        random.z = Random.Range(zMin, zMax);

        GameObject childObj = Instantiate(KomodoPrefab, random, Quaternion.identity);
        childObj.transform.parent = gameObject.transform;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
