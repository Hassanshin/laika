﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventScenesLevel : MonoBehaviour {

    public int Scene = 5;

    public void AddScene()
    {
        Debug.Log("Scene execute");
        SceneManager.LoadScene(Scene, LoadSceneMode.Additive);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AddScene();
        }
    }
}
