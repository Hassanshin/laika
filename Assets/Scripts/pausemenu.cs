﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pausemenu : MonoBehaviour {
    public Transform canvas;
    public Transform player;

    public FPSCamera fps;
    public TPSCamera tps;

    public void backtoMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void resume()
    {
        
            canvas.gameObject.SetActive(false);
            Time.timeScale = 1;
            GameObject.Find("Laika").GetComponent<PlayerControl>().disableControl = false;
        
    }


    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (canvas.gameObject.activeInHierarchy == false)
            {
                canvas.gameObject.SetActive(true);
                Time.timeScale = 0;
                GameObject.Find("Laika").GetComponent<PlayerControl>().disableControl = true;
                fps.lockCursor = false;
                tps.lockCursor = false;
            }
            else {
                canvas.gameObject.SetActive(false);
                Time.timeScale = 1;
                GameObject.Find("Laika").GetComponent<PlayerControl>().disableControl = false;
                fps.lockCursor = true;
                tps.lockCursor = true;
            }

        }


	}





}
