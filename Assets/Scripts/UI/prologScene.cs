﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class prologScene : MonoBehaviour {

    
    public RawImage GambarRaw;
    public VideoPlayer VideoPlayer;
    public AudioSource VideoAuido;

    public float delayVideo;

    private void Start()
    {
        
        StartCoroutine(StartVideo());
    }

    IEnumerator StartVideo()
    {
        VideoPlayer.Prepare();
        WaitForSeconds waitforseconds = new WaitForSeconds(1);
        while (!VideoPlayer.isPrepared)
        {
            yield return waitforseconds;
            break;
        }
        GambarRaw.texture = VideoPlayer.texture;
        VideoPlayer.Play();
        VideoAuido.Play();
        Debug.Log("video");
        Invoke("delVideo", delayVideo);

    }

    void delVideo()
    {
        SceneManager.LoadScene(1);
    }
}
