﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.Video;
using UnityEngine.UI;

public class mainMenu : MonoBehaviour {

    public AudioMixer audioMixer;

    public Dropdown resolutionDropdown;
    Resolution[] resolutions;

    public GameObject VideoGameobject;
    public RawImage bumperRaw;
    public VideoPlayer bumperVideo;
    public AudioSource bumperMusic;

    public float delayVideo;

    private void Start()
    {
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolution = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width &&
                resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolution = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolution;
        resolutionDropdown.RefreshShownValue();

        StartCoroutine(StartVideo());
    }

    IEnumerator StartVideo()
    {
        bumperVideo.Prepare();
        WaitForSeconds waitforseconds = new WaitForSeconds(1);
        while (!bumperVideo.isPrepared)
        {
            yield return waitforseconds;
            break;
        }
        bumperRaw.texture = bumperVideo.texture;
        bumperVideo.Play();
        bumperMusic.Play();
        Debug.Log("video");
        Invoke("delVideo", delayVideo);

    }

    void delVideo()
    {
        VideoGameobject.SetActive(false);
    }

    public void setResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void NewGame()
    {
        SceneManager.LoadScene(4);
        PlayerPrefs.SetInt("sav", -5);
    }

    public void Continue()
    {
        SceneManager.LoadScene(1);
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
        
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void ExitBtn()
    {
        Application.Quit();
    }
}
