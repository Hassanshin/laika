﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestCollisonKomodoTangkap : QuestCollision {

    public ObjectiveChar quest;

    public override void OnTriggerEnter(Collider other)
    {
        if(quest.komodoCount >= 15)
        {
            base.OnTriggerEnter(other);
        }
    }
}
