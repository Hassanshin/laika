﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Permata : MonoBehaviour {

    public GameObject toolTip;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            toolTip.SetActive(true);
            if (Input.GetKey(KeyCode.E))
            {
                ObjectiveChar tujuan = other.GetComponent<ObjectiveChar>();
                tujuan.keyCount += 1;

                toolTip.SetActive(!true);
                Destroy(transform.gameObject);
            }
        }
       
    }
}
