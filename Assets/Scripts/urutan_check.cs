﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class urutan_check : MonoBehaviour {

    [Range(1,4)]
    public int angkaBenar;

    [Range(1, 4)]
    public int angkaSekarang;

    public bool isTrue;
    public AudioSource sound;

    public void Rotasi()
    {
        transform.Rotate(90, 0, 0);
        
        if (angkaSekarang < 4)
        {
            angkaSekarang++;
            if (sound.isPlaying)
            {
                sound.Stop();
                sound.Play();
            } else
            {
                sound.Play();
            }
        } else
        {
            angkaSekarang = 1;
        }
    }

    public void Update()
    {
        if (angkaBenar == angkaSekarang)
        {

            isTrue = true;
        } else
        {
            isTrue = !true;
        }
    }
}
