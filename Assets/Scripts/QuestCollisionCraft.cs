﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestCollisionCraft : MonoBehaviour {

    public string[] questGet;
    public string[] questDone;

    public GameObject[] spawnObject;
    public GameObject[] removObject;

    public QuestScript questUI;

    [Header("Save")]
    public int savingNum = -5;

    [Header("Special")]
    public bool destroyAfterTrigger = true;
    public bool cameraModeSwitch;
    public bool modeFPS;

    GameObject Player;

    private void Start()
    {
        Player = GameObject.Find("Laika");
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        if (Player.GetComponent<ObjectiveChar>().C_Obat > 0)
        {
            if (other.tag == "Player")
            {
                if (savingNum > -1)
                {
                    PlayerPrefs.SetInt("sav", savingNum);
                    Debug.Log("Saving +++ " + savingNum);
                }

                if (cameraModeSwitch)
                {
                    other.GetComponent<PlayerControl>().FPSMode = modeFPS;
                }

                for (int i = 0; i < questDone.Length; i++)
                {
                    questUI.QuestSelesai(questDone[i]);
                }

                for (int i = 0; i < questGet.Length; i++)
                {
                    questUI.AddQuest(questGet[i]);
                }

                for (int i = 0; i < spawnObject.Length; i++)
                {
                    spawnObject[i].SetActive(true);
                }

                for (int i = 0; i < removObject.Length; i++)
                {
                    removObject[i].SetActive(!true);
                }

                if (destroyAfterTrigger)
                    Destroy(gameObject);
            }
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        for (int i = 0; i < spawnObject.Length; i++)
        {
            spawnObject[i].SetActive(!true);
        }
    }
}
