﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveChar : MonoBehaviour {

    [Header("Quest Mantab")]
    public int keyCount;
    public int komodoCount;

    public bool sudahMengambilPeta;

    ItemInventory tas;

    [Header("Items Count")]
    public int totalInventory;

    public int t_ranting;
    public int t_rumput;

    public int t_bungaMerah;
    public int t_bungaBiru;
    public int t_bungaKuning;

    public int daunHijau;
    public int daunHitam;

    public int C_Makan;
    public int C_Obat;

    [TextArea]
    [Tooltip("A string using the TextArea attribute")]
    [SerializeField]
    private string descriptionTextArea;

    [Header("Items List")]
    public Item Ranting;
    public Item Rumput;
    public Item BungaMerah;
    public Item[] Bunga;
    public Item[] Ramuan;

    [Header("Craft List")]
    public Item M_Pesut;
    public Item Obat;

    [Header("UI")]
    public Text komodoCountText;

    public GameObject CraftUI_parent;
    public GameObject[] CraftUI_text;

    QuestScript quest;

    bool a = false;
    bool b = false;

    int x = 0;

    private void Start()
    {
        tas = GameObject.Find("Inventory").GetComponent<ItemInventory>();
        quest = GameObject.Find("Quest List").GetComponent<QuestScript>();
    }

    void Update()
    {
        questCounter();
        Counter();
        CraftingUI();

        if (komodoCount > 0)
        {
            if(x < 4)
            {
                komodoCountText.gameObject.transform.parent.gameObject.SetActive(true);
                x = 10;
            }
            
            komodoCountText.text = komodoCount + "";
        }
    }

    void questCounter()
    {
        if( quest != null)
        {
            
            if (keyCount >= 3 && !a)
            {
                quest.QuestSelesai("Cari Kunci");
                a = true;
            }

            
            if (komodoCount >= 15 && !b)
            {
                quest.QuestSelesai("Tangkap Anak Komodo");
                quest.AddQuest("Kembali ke Ibu Komodo");

                

                b = true;
            }
        }
    }

    public void CraftingUI()
    {
        if(t_ranting >= 2 && t_rumput >= 1)
        {
            CraftUI_text[0].SetActive(true);
        } else
        {
            CraftUI_text[0].SetActive(!true);
        }

        if (t_ranting >= 2 && t_rumput >= 1)
        {
            CraftUI_text[1].SetActive(true);
        }
        else
        {
            CraftUI_text[1].SetActive(!true);
        }

        //jingga
        if (t_bungaMerah >= 2 && t_bungaKuning >= 2)
        {
            CraftUI_text[2].SetActive(true);
        }
        else
        {
            CraftUI_text[2].SetActive(!true);
        }

        //hijau
        if (t_bungaBiru >= 2 && t_bungaKuning >= 2)
        {
            CraftUI_text[3].SetActive(true);


            C_Obat++;
        }
        else
        {
            CraftUI_text[3].SetActive(!true);
        }

        //ungu
        if (t_bungaBiru >= 2 && t_bungaMerah >= 2)
        {
            CraftUI_text[4].SetActive(true);
        }
        else
        {
            CraftUI_text[4].SetActive(!true);
        }
    }

    public void Craft(int index)
    {
        switch (index)
        {
            case 0:
                ItemInventory.instance.Add(M_Pesut);

                ItemInventory.instance.Rem(Ranting);
                ItemInventory.instance.Rem(Ranting);
                ItemInventory.instance.Rem(Rumput);

                Debug.Log("Crafting " + index);
                break;
            case 1:
                ItemInventory.instance.Add(Obat);

                ItemInventory.instance.Rem(Rumput);
                ItemInventory.instance.Rem(BungaMerah);
                ItemInventory.instance.Rem(BungaMerah);

                Debug.Log("Crafting " + index);
                break;
            case 2:
                ItemInventory.instance.Add(Ramuan[0]);

                ItemInventory.instance.Rem(Bunga[0]);
                ItemInventory.instance.Rem(Bunga[0]);

                ItemInventory.instance.Rem(Bunga[1]);
                ItemInventory.instance.Rem(Bunga[1]);
                break;
            case 3:
                ItemInventory.instance.Add(Ramuan[1]);

                ItemInventory.instance.Rem(Bunga[2]);
                ItemInventory.instance.Rem(Bunga[2]);

                ItemInventory.instance.Rem(Bunga[1]);
                ItemInventory.instance.Rem(Bunga[1]);
                break;
            case 4:
                ItemInventory.instance.Add(Ramuan[2]);

                ItemInventory.instance.Rem(Bunga[2]);
                ItemInventory.instance.Rem(Bunga[2]);

                ItemInventory.instance.Rem(Bunga[0]);
                ItemInventory.instance.Rem(Bunga[0]);
                break;
            default:
                Debug.Log("Default");
                break;
        }
    }

    public void Counter()
    {
        totalInventory = tas.items.Count;

        List<Item> rantingList = tas.items.FindAll(s => s.name == "Ranting");
        t_ranting = rantingList.Count;

        List<Item> rumputList = tas.items.FindAll(s => s.name == "Rumput");
        t_rumput = rumputList.Count;

        List<Item> bungaMerahList = tas.items.FindAll(s => s.name == "Bunga Merah");
        t_bungaMerah = bungaMerahList.Count;

        List<Item> bungaKuningList = tas.items.FindAll(s => s.name == "Bunga Kuning");
        t_bungaKuning = bungaKuningList.Count;

        List<Item> bungaBiruList = tas.items.FindAll(s => s.name == "Bunga Biru");
        t_bungaBiru = bungaBiruList.Count;

        //if (tas.items.Count > 0)
        //{
        //    Debug.Log(tas.items[0].name);
        //    Debug.Log("Ranting = " + rantingList.Count);
        //}
    }

    
}
