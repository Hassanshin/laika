﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour {

    public bool disableControl;

    [Header("Stats")]
    public float walkSpeed = 2;
    public float runSpeed = 5;
    public float jumpHeight = 1;
    public float gravity = -12;

    [Range(0, 1)]
    public float airControlPercent;

    public float turnSmoothTime = 0.2f;
    float turnSmoothVelocity;

    public float speedSmoothTime = 0.1f;
    float speedSmoothVelocity;
    public float currentSpeed;
    float velocityY;

    [Header("Mekanik")]
    public bool membawa;
    public GameObject yangdibawa;

    public bool disemak;
    public bool diAir;
    public bool diTempatTopeng;
    public bool diPintuLabirin;
    public bool diTempatTangram;

    public GameObject percikanAir;

    int x;
    int y;

    float counter = 0;

    [Header("Camera")]
    public bool FPSMode;

    public GameObject TPSCamera;
    public GameObject FPSCamera;

    Animator animator;
    //Transform cameraT;
    CharacterController controller;

    public GameObject miniMap;

    private ObjectiveChar objective;

    void Start()
    {
        animator = GetComponent<Animator>();
        //cameraT = Camera.main.transform;
        controller = GetComponent<CharacterController>();

        objective = GetComponent<ObjectiveChar>();

        x = Screen.width / 2;
        y = Screen.height / 2;
    }

    void Update()
    {
        if (!disableControl)
        {
            //air
            percikanAir.SetActive(diAir);

            Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            Vector2 inputDir = input.normalized;

            //buang tanda seru untuk membalik fungsi
            bool running = !Input.GetKey(KeyCode.LeftShift);


            if (Input.GetKey(KeyCode.LeftControl))
            {
                runSpeed = 50;
            } else
            {
                runSpeed = 7;
            }


            if (FPSMode)
            {
                DisableFPS(!true);
                FPSMove(inputDir, running);

                if (objective.sudahMengambilPeta)
                    miniMap.SetActive(false);
            } else
            {
                DisableFPS(true);
                TPSMove(inputDir, running);

                if (objective.sudahMengambilPeta)
                    miniMap.SetActive(true);
                
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }

            //E_TahanVoid();
            TombolUnik();


            if (yangdibawa != null)
            {
                membawa = true;

            }
            else
            {
                membawa = false;
            }

            //animasi
            float animationSpeedPercent = ((running) ? currentSpeed / runSpeed : currentSpeed / walkSpeed * .5f);
            animator.SetFloat("speedPercent", animationSpeedPercent);
            animator.SetFloat("y", velocityY);
            animator.SetBool("isGrounded", controller.isGrounded);

        }

    }

    void TombolUnik()
    {
        
        if (Input.GetKey(KeyCode.F)) 
        {

            if (counter < 1.0f)
            {
                counter += Time.deltaTime;
            } else
            {
                FPSMode = !FPSMode;
                counter = 0;
            }
            
        } 

        if (Input.GetKey(KeyCode.C))
        {
            if (yangdibawa != null && yangdibawa.tag == "Kunci")
            {
                PuzzleKunci kunciScript = yangdibawa.GetComponent<PuzzleKunci>();

                yangdibawa.GetComponent<Rigidbody>().isKinematic = false;
                yangdibawa.GetComponent<BoxCollider>().enabled = true;

                yangdibawa = null;

                kunciScript.emak = null;
            }
        }

        if (Input.GetKey(KeyCode.E))
        {
            
        }
    }


    private void DisableFPS(bool state)
    {
        FPSCamera.SetActive(!state);
        TPSCamera.SetActive(state);
    }

    void FPSMove(Vector2 inputDir, bool running)
    {
        //if (inputDir != Vector2.zero)
        //{
        //    float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg + cameraT.eulerAngles.y;
        //    transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, GetModifiedSmoothTime(turnSmoothTime));
        //}

        float targetSpeed = ((running) ? runSpeed : walkSpeed) * inputDir.magnitude;
        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, GetModifiedSmoothTime(speedSmoothTime));

        //gravitasi
        velocityY += Time.deltaTime * gravity;

        Vector3 forwardMov = transform.forward * inputDir.y * currentSpeed;
        Vector3 rightMov = transform.right * inputDir.x * currentSpeed;

        Vector3 velocity = rightMov + forwardMov + Vector3.up * velocityY;

        controller.Move(velocity * Time.deltaTime);

        

        if (controller.isGrounded)
        {
            velocityY = 0;
        }

        
    }

    void TPSMove(Vector2 inputDir, bool running)
    {
        if (inputDir != Vector2.zero)
        {
            float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg + TPSCamera.transform.eulerAngles.y;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, GetModifiedSmoothTime(turnSmoothTime));
        }
        
        float targetSpeed = ((running) ? runSpeed : walkSpeed) * inputDir.magnitude;
        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, GetModifiedSmoothTime(speedSmoothTime));

        //gravitasi
        velocityY += Time.deltaTime * gravity;

        Vector3 velocity = transform.forward * currentSpeed + Vector3.up * velocityY;

        controller.Move(velocity * Time.deltaTime);
        currentSpeed = new Vector2(controller.velocity.x, controller.velocity.z).magnitude;

        if (controller.isGrounded)
        {
            
            velocityY = 0;
        }
    }

    void Jump()
    {
        if (controller.isGrounded)
        {
            
            float jumpVelocity = Mathf.Sqrt(-2 * gravity * jumpHeight);
            velocityY = jumpVelocity;
        } 
    }

    float GetModifiedSmoothTime(float smoothTime)
    {
        if (controller.isGrounded)
        {
            return smoothTime;
        }

        if (airControlPercent == 0)
        {
            return float.MaxValue;
        }

        return smoothTime / airControlPercent;
    }

}
