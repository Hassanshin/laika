﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCamera : MonoBehaviour {

    public bool lockCursor;
    public bool disableControl;
    public float mouseSensitivity = 10;

    public Transform player;

    float xAxisClamp;

    float yaw;
    float pitch;

    [Header("Bobbing")]
    public float timer = 0.0f;
    public float bobbingSpeed = 0.18f;
    float bobbingSpeedWalk;
    public float bobbingAmount = 0.2f;
    float midpoint;

    private void Awake()
    {
        bobbingSpeedWalk = bobbingSpeed / 3;
        xAxisClamp = 0.0f;
        midpoint = transform.position.y;
        Debug.Log(midpoint);
    }



    void Update()
    {
        if (lockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        BobbingVoid();
    }

    void BobbingVoid()
    {
        float waveslice = 0.0f;
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 cSharpConversion = transform.localPosition;
        //cSharpConversion.y = 0;

        if (Mathf.Abs(horizontal) == 0 && Mathf.Abs(vertical) == 0)
        {
            timer = 0.0f;
        }
        else
        {
            waveslice = Mathf.Sin(timer);

            if (Input.GetKey(KeyCode.LeftShift))
            {
                timer = timer + bobbingSpeedWalk;
            } else
            {
                timer = timer + bobbingSpeed;
            }
            
            if (timer > Mathf.PI * 2)
            {
                timer = timer - (Mathf.PI * 2);
            }
        }
        if (waveslice != 0)
        {
            float translateChange = waveslice * bobbingAmount;
            float totalAxes = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
            totalAxes = Mathf.Clamp(totalAxes, 0.0f, 1.0f);
            translateChange = totalAxes * translateChange;
            cSharpConversion.y = midpoint + translateChange;
        }
        else
        {
            cSharpConversion.y = midpoint;
        }

        transform.localPosition = cSharpConversion;
    }

    void LateUpdate()
    {
        if (!disableControl)
        {
            CameraRotation();
        }

    }

    void CameraRotation()
    {
        yaw = Input.GetAxis("Mouse X") * mouseSensitivity;
        pitch = Input.GetAxis("Mouse Y") * mouseSensitivity;

        xAxisClamp += pitch;

        if (xAxisClamp > 90)
        {
            xAxisClamp = 90.0f;
            pitch = 0.0f;
            ClamXAxisRotationToValue(270.0f);
        }
        else
        if (xAxisClamp < -60)
        {
            xAxisClamp = -60.0f;
            pitch = 0.0f;
            ClamXAxisRotationToValue(60.0f);
        }

        transform.Rotate(Vector3.left * pitch);
        player.Rotate(Vector3.up * yaw);
    }

    void ClamXAxisRotationToValue(float value)
    {
        Vector3 eulerRotation = transform.eulerAngles;
        eulerRotation.x = value;
        transform.eulerAngles = eulerRotation;
    }
}
