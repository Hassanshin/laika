﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactable : MonoBehaviour {

    public GameObject UI_Tooltip;
    Text tooltip_text;

    public bool customText = false;

    public string tambahan = "mengambil";

    public Item item;

    [Header("Quest")]
    public bool questing;
    private QuestScript questUI;

    public string[] questDone;
    public string[] questGet;

    public GameObject[] spawnObject;
    public GameObject[] removObject;

    [Header("Special")]
    public bool questPeta;

    private void Awake()
    {
        //UI_Tooltip = GameObject.Find("Panel_ToolTip");
    }

    private void Start()
    {
        
        tooltip_text = UI_Tooltip.gameObject.transform.GetChild(0).GetComponent<Text>();

        if(questing)
        questUI = GameObject.Find("Quest List").GetComponent<QuestScript>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (customText)
            {
                tooltip_text.text = tambahan;
            }
            else if(item != null)
            {
                tooltip_text.text = "Tekan E untuk " + tambahan + " " + item.name;
            }              
            else
            {
                tooltip_text.text = "Tekan E untuk " + tambahan;
            }
                

            UI_Tooltip.SetActive(true);

            if (Input.GetButton("Use"))
            {
                Interact();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            UI_Tooltip.SetActive(false);
            
        }
    }

    public virtual void Interact()
    {   
        UI_Tooltip.SetActive(false);

        if (questPeta)
        {
            GameObject.Find("Laika").GetComponent<ObjectiveChar>().sudahMengambilPeta = true;
            
        }
            

        if (questing)
        {
            for (int i = 0; i < questDone.Length; i++)
            {
                questUI.QuestSelesai(questDone[i]);
            }

            for (int i = 0; i < questGet.Length; i++)
            {
                questUI.AddQuest(questGet[i]);
            }

            for (int i = 0; i < spawnObject.Length; i++)
            {
                spawnObject[i].SetActive(true);
            }

            for (int i = 0; i < removObject.Length; i++)
            {
                removObject[i].SetActive(!true);
            }
        }
        //Debug.Log("Interacting with " + item.name);
    }
}
