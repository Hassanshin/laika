﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Komodo : MonoBehaviour {

    public bool evadeMode;
    
    
    public float speed;

    //public 

    Transform respawn;
    Vector3 tempatAwal;
    Animator anim;
    NavMeshAgent agent;

    public enum State
    {
        PATROL, CHASE, IDLE, KAGET, PULANG, FLEE
    }

    public State state;
    private bool alive;

    [Header("Flee")]
    public float jarakMin;

    [Header("Chasing")]
    public float chaseSpeed = 1f;
    public GameObject target;

    [Header("Kaget")]
    public float x = 0;
    public GameObject target_Kaget;

    void Start () {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();

        //agent.updatePosition = true;
        //agent.updatePosition = false;

        state = Komodo.State.IDLE;

        alive = true;

        awalan();
        //Invoke("awalan", 2f);

        StartCoroutine("FSM");
    }

    void awalan()
    {
        tempatAwal = transform.position;
    }

    IEnumerator FSM()
    {
        while (alive)
        {
            //PATROL, CHASE, IDLE, KAGET, PULANG
            switch (state)
            {
                case State.PATROL:
                    Patrol();
                    break;
                case State.CHASE:
                    Chase();
                    break;
                case State.IDLE:
                    Idle();
                    break;
                case State.KAGET:
                    Kaget();
                    break;
                case State.PULANG:
                    Pulang();
                    break;
                case State.FLEE:
                    Flee();
                    break;
                default:
                    Idle();
                    break;
            }
            yield return null;
        }
    }

    void Patrol()
    {
        //Debug.Log("FSM Patrol");
    }

    void Flee()
    {
        Vector3 newPos;

        if (target != null)
        {
            //float jarak = Vector3.Distance(transform.position, target.transform.position);

            ////lari
            //if (jarak < jarakMin)
            //{
                //disini
            //}
            //else
            //{
            //    state = Komodo.State.IDLE;
            //}

            Vector3 jarakMenujuPlayer = transform.position - target.transform.position;

            newPos = transform.position + jarakMenujuPlayer;
            newPos = newPos + new Vector3(Random.Range(-2,2), 0, Random.Range(-2,2));

            if (Vector3.Distance(transform.position, newPos) > 1)
            {
                agent.SetDestination(newPos);
            }

        } else
        {
            state = Komodo.State.IDLE;

            newPos = Vector3.zero;
        }
    }

    void Chase()
    {
        if (target != null && !target.GetComponent<PlayerControl>().disemak)
        {
            //Debug.Log("FSM Chase");
            
            agent.SetDestination(target.transform.position);

        } else
        {
            state = Komodo.State.IDLE;
        }

        anim.SetBool("idle", !true);
    }

    void Idle()
    {
        //Debug.Log("FSM Idle");
        if (!evadeMode)
        {
            if (target != null)
            {
                state = Komodo.State.CHASE;
            }
            else if (target_Kaget != null)
            {
                state = Komodo.State.KAGET;
            }
            else if (Vector3.Distance(transform.position, tempatAwal) > 1)
            {
                state = Komodo.State.PULANG;
            }
        }
        else
        {
            if (target != null)
            {
                state = Komodo.State.FLEE;
            }
            else if (target_Kaget != null)
            {
                state = Komodo.State.KAGET;
            }
        }
        

        anim.SetBool("idle", true);
    }

    void Kaget()
    {
        
        float xLimit = 1.0f;
        //Debug.Log("FSM Kaget");
        
        if (!evadeMode)
        {
            if (target_Kaget != null)
            {
                if (x < xLimit)
                {
                    x += Time.deltaTime;

                    transform.LookAt(target_Kaget.transform);
                }
                else
                {
                    state = Komodo.State.CHASE;
                    target = target_Kaget;
                    target_Kaget = null;

                    x = 0;
                }
            }
            else
            {
                state = Komodo.State.IDLE;
                x = 0;
            }
        }
        else
        {
            if (target_Kaget != null)
            {
                if (x < xLimit)
                {
                    x += Time.deltaTime;

                    transform.LookAt(target_Kaget.transform);
                }
                else
                {
                    state = Komodo.State.FLEE;
                    target = target_Kaget;
                    target_Kaget = null;

                    x = 0;
                }
            }
            else
            {
                state = Komodo.State.IDLE;
                x = 0;
            }
        }

        anim.SetBool("idle", !true);
    }

    void Pulang()
    {
        //Debug.Log("FSM Pulang");
        if (Vector3.Distance(transform.position, tempatAwal) < 1)
        {
            state = Komodo.State.IDLE;
        } else
        
        if (target != null)
        {
            state = Komodo.State.CHASE;
        }

        else
        {
            agent.SetDestination(tempatAwal);
        }

        anim.SetBool("idle", !true);
    }
	
	void Update () {
		
	}

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (evadeMode)
            {
                
            }
            else
            { //player kalah dan kembali ke tempat ini

                if (Random.Range(1, 10) % 2 == 0)
                {
                    other.transform.position = new Vector3(390, 0.5f, -227);
                }
                else
                {
                    other.transform.position = new Vector3(375, 0.5f, -222);
                }

            }
        }
    }
}
