﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable_Items : Interactable {

    public override void Interact()
    {
        base.Interact();

        PickUp();
    }

    private void PickUp()
    {

        Debug.Log("Picking up item " + item.name);

        //add to inventtory
        bool wasPickedUp = ItemInventory.instance.Add(item);

        if(wasPickedUp)
        Destroy(gameObject);
    }
}
