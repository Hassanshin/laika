﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventScenes : MonoBehaviour {

    public int Scene = 2;
    public float Delay = 10f;

    public GameObject[] disabledCamera;

    GameObject player;

    public void Start()
    {
        player = GameObject.Find("Laika");
    }

    public void AddScene()
    {
        Debug.Log("Scene execute");
        SceneManager.LoadScene(Scene, LoadSceneMode.Additive);
        CameraIlang();
        Invoke("DelScene", Delay);
    }

    public void CameraIlang()
    {
        player.GetComponent<PlayerControl>().disableControl = true;
        for (int i = 0; i < disabledCamera.Length; i++)
        {
            if(disabledCamera[i].activeSelf)
                disabledCamera[i].SetActive(false);
        }
    }

    public void CameraTidakIlang()
    {
        player.GetComponent<PlayerControl>().disableControl = !true;
        for (int i = 0; i < disabledCamera.Length; i++)
        {
            if (!disabledCamera[i].activeSelf)
                disabledCamera[i].SetActive(true);
        }
    }

    public void DelScene()
    {
        Debug.Log("Scene delete");
        CameraTidakIlang();
        SceneManager.UnloadSceneAsync(Scene);
        Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AddScene();
        }
    }
}
