﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject {

    new public string name = "New Item";
    public Sprite icon = null;
    public bool isDeaultItem = false;
    public int topengNum = -5;
    public bool isKunci = false;
    public int tangramNum = -5;

    Item itemSelf;

    public virtual void use()
    {
        // use the items
        if (isKunci)
        {
            PlayerControl player = GameObject.Find("Laika").GetComponent<PlayerControl>();
            Pintu tempatPintuin = GameObject.Find("trigger_pintu").GetComponent<Pintu>();

            itemSelf = this;

            if (player.diPintuLabirin)
            {
                tempatPintuin.stored += 1;
                ItemInventory.instance.Rem(itemSelf);
            }
        }

        if (topengNum > -1)
        {
            PlayerControl player = GameObject.Find("Laika").GetComponent<PlayerControl>();
            TempatTopeng tempatTopengin = GameObject.Find("TempatTopeng_mantab").GetComponent<TempatTopeng>();

            itemSelf = this;

            Debug.Log("Using " + name);
            if (player.diTempatTopeng)
            {
                switch (topengNum)
                {
                    case 0:
                        tempatTopengin.TopengDayak.SetActive(true);
                        ItemInventory.instance.Rem(itemSelf);
                        Debug.Log("Memberi topeng");
                        break;
                    case 1:
                        tempatTopengin.TopengBali.SetActive(true);
                        ItemInventory.instance.Rem(itemSelf);
                        Debug.Log("Memberi topeng");
                        break;
                    default:
                        break;
                }
            } else
            {
                Debug.Log("Tidak di Tempat topeng");
            }
        }
        
        if (tangramNum > -1)
        {
            PlayerControl player = GameObject.Find("Laika").GetComponent<PlayerControl>();
            TangramShow tempatTangram = GameObject.Find("gram").GetComponent<TangramShow>();

            itemSelf = this;

            Debug.Log("Using " + name);
            if (player.diTempatTangram)
            {
                if(tangramNum == tempatTangram.urutanKe)
                {
                    tempatTangram.urutanKe++;
                    tempatTangram.tangramAda[tangramNum] = true;
                    ItemInventory.instance.Rem(itemSelf);
                    Debug.Log("Memberi tangram");
                } else
                {
                    Debug.Log("tangram salah");
                }
                
                
                
            }
            else
            {
                Debug.Log("Tidak di Tempat topeng");
            }
        }
    }

}
