﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KomodoTrigger : MonoBehaviour {

    Komodo parent;

    [Header("Player")]
    public float speedMin = 2.1f;
    public float jarakMin = 3f;
    public float jarak;

    private void Start()
    {
        parent = GetComponentInParent<Komodo>();
    }

    private void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other != null)
        {
            if (other.tag == "Player")
            {
                PlayerControl playerscript = other.GetComponent<PlayerControl>();
                ObjectiveChar playerobj = other.GetComponent<ObjectiveChar>();

                

                if (playerscript.disemak)
                {
                    parent.target = null;
                } 
                else
                if (playerscript.currentSpeed > speedMin && !playerscript.disemak)
                {
                    parent.target = other.gameObject;
                } 

            }
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            parent.target = null;
        }
    }
}
