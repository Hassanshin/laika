﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KomodoTriggerSmall : MonoBehaviour {

    bool evadeMode;

    Komodo parent;


    private void Start()
    {
        parent = GetComponentInParent<Komodo>();
        evadeMode = parent.evadeMode;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other != null)
        {
            if (other.tag == "Player")
            {
                PlayerControl playerscript = other.GetComponent<PlayerControl>();
                ObjectiveChar playerobj = other.GetComponent<ObjectiveChar>();

                if (!playerscript.disemak)
                {
                    parent.target_Kaget = other.gameObject;
                }



                if (evadeMode && Input.GetButton("Use"))
                {
                    

                    playerobj.komodoCount += 1;
                    Destroy(transform.parent.gameObject);
                }
            }
        }

        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            parent.target_Kaget = null;
        }
    }
}
