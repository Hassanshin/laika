﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepStone : MonoBehaviour {

    [Header("Awalan")]
    public GameObject partikelBuka;
    public GameObject partikelTutup;
    public float speed;

    Material matAwal;
    Vector3 letakAwal;
    Vector3 letakBawah;

    bool on;

    [Header("Efek dari StepStone")]
    public GameObject[] TogglePintu;
    public GameObject[] OffPintu;
    public GameObject[] OnPintu;

    public GameObject[] unlockDel;
    public GameObject[] lockActive;

    Renderer kangRender;

    private void Start()
    {
        kangRender = GetComponent<Renderer>();

        matAwal = kangRender.material;

        letakAwal = transform.position;

        letakBawah = transform.position;
        letakBawah.y = letakBawah.y - 0.45f;

        Off(); //awal selalu off
    }

    private void Update()
    {
        if (on)
        {
            On();
        } else 
        {
            Off();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            on = !on;
            efekKePintuToggle(on);

            if (!on)
            {
                efekKePintuOn(!true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            //on = false;
        }
    }

    void On()
    {
        //pindah tempat
        transform.position = Vector3.Lerp(transform.position, letakBawah, speed);

        partikelBuka.SetActive(true);
        partikelTutup.SetActive(!true);

        efekKePintuOn(true);
        efekKePintuOff(!true);
    }

    void Off()
    {
        //pindah tempat
        transform.position = Vector3.Lerp(transform.position, letakAwal, speed);
        //transform.position = letakAwal;

        partikelBuka.SetActive(!true);
        partikelTutup.SetActive(true);

    }

    void efekKePintuToggle(bool nyala)
    {
        for (int i = 0; i < TogglePintu.Length; i++)
        {
            if (TogglePintu[i] != null)
            {
                PintuStepStone anim = TogglePintu[i].GetComponent<PintuStepStone>();
                anim.nyala = !anim.nyala;
            }
        }
    }

    void efekKePintuOn(bool state)
    {
        for (int i = 0; i < OnPintu.Length; i++)
        {
            if (OnPintu[i] != null)
            {
                PintuStepStone anim = OnPintu[i].GetComponent<PintuStepStone>();
                anim.nyala = state;
            }
        }

        for (int x = 0; x < unlockDel.Length; x++)
        {
            if(unlockDel[x] != null)
            {
                unlockDel[x].SetActive(state);
            }
        }
    }

    void efekKePintuOff(bool state)
    {
        for (int i = 0; i < OffPintu.Length; i++)
        {
            if (OffPintu[i] != null)
            {
                PintuStepStone anim = OffPintu[i].GetComponent<PintuStepStone>();
                anim.nyala = state;
            }
        }

        for (int x = 0; x < lockActive.Length; x++)
        {
            if (lockActive[x] != null)
            {
                lockActive[x].SetActive(state);
            }
        }
    }
}
