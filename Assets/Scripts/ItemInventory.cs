﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInventory : MonoBehaviour {

#region singelton

    public static ItemInventory instance;



    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("lebih dari 1 inventory!!");
            return;
        }
        instance = this;
    }

    #endregion

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int space = 12;

    public List<Item> items = new List<Item>();

    public bool Add(Item item)
    {
        if (!item.isDeaultItem)
        {
            if (items.Count >= space)
            {
                Debug.Log("tas penuh");
                return false;
            }
            items.Add(item);

            if(onItemChangedCallback != null)
                onItemChangedCallback.Invoke();
        }

        return true;
    }

    public void Rem(Item item)
    {
        items.Remove(item);

        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }


}
