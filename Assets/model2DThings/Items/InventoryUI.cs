﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class InventoryUI : MonoBehaviour {

    ItemInventory inventory;
    public GameObject inventoryGame;
    public GameObject mapGame;

    public Transform itemsParent;

    public TPSCamera camera1script;
    public FPSCamera camera2script;

    PlayerControl player;

    InventorySlot[] slots;
    bool x;
    public int a = 0;
    public int b = 0;

    public GameObject pauseMenu;


    [Header("Item")]
    public Item questPetaItem;
    

    void Start () {
        inventory = ItemInventory.instance;
        inventory.onItemChangedCallback += UpdateUI;

        slots = itemsParent.GetComponentsInChildren<InventorySlot>();

        player = GameObject.Find("Laika").GetComponent<PlayerControl>();

    }
	
	void Update () {

        if (Input.GetButtonDown("Inventory"))
        {
            if (b < 5)
            {
                GameObject.Find("tutor_tas").SetActive(false);
                b = 10;
            }

            inventoryGame.SetActive(!inventoryGame.activeSelf);

            //camera1script.lockCursor = !camera1script.lockCursor;
            //camera2script.lockCursor = !camera2script.lockCursor;

            x = inventoryGame.activeSelf;

            LockCursor(x);
        }

        if (Input.GetButtonDown("Map"))
        {
            if (player.gameObject.GetComponent<ObjectiveChar>().sudahMengambilPeta)
            {
                // menghilangkan tutor map
                
                if(a < 5)
                {
                    
                    GameObject tutorMapG = GameObject.Find("tutor_map").gameObject;

                    if (tutorMapG.gameObject.activeSelf)
                    {
                        tutorMapG.SetActive(false);
                        ItemInventory.instance.Rem(questPetaItem);

                    }
                    a = 10;
                }

                mapGame.SetActive(!mapGame.activeSelf);

                //camera1script.lockCursor = !camera1script.lockCursor;
                //camera2script.lockCursor = !camera2script.lockCursor;

                x = mapGame.activeSelf;

                LockCursor(x);
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf);

            x = pauseMenu.activeSelf;

            LockCursor(x);
        }
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    void LockCursor(bool state)
    {
        camera1script.lockCursor = !state;
        camera2script.lockCursor = !state;

        camera1script.disableControl = state;
        camera2script.disableControl = state;
        player.disableControl = state;
    }

    void UpdateUI()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if(i < inventory.items.Count)
            {
                slots[i].AddItem(inventory.items[i]);
            } else
            {
                slots[i].ClearSlot();
            }
        }
    }
}
