﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class savManager : MonoBehaviour {

    public int savingNum;

    [Header("Saving 0")]
    public string[] questDone0;
    public string[] questGet0;
    public GameObject[] Remove0;
    public GameObject[] Spawn0;

    GameObject player;
    QuestScript questUI;
    ObjectiveChar objektif;
    InventoryUI tas;

	void Start () {
        savingNum = PlayerPrefs.GetInt("sav");

        player = GameObject.Find("Laika");
        objektif = GameObject.Find("Laika").GetComponent<ObjectiveChar>();
        tas = GameObject.Find("UI Canvas").GetComponent<InventoryUI>();
        questUI = GameObject.Find("Quest List").GetComponent<QuestScript>();

        Exectue();
	}

    private void Update()
    {
        if (Input.GetKey(KeyCode.Backspace))
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetInt("sav", -5);
        }
    }

    void Exectue()
    {
        switch (savingNum)
        {
            case 0:
                Load0();

                for (int i = 0; i < questDone0.Length; i++)
                {
                    questUI.QuestSelesai(questDone0[i]);

                }

                for (int i = 0; i < questGet0.Length; i++)
                {
                    questUI.AddQuest(questGet0[i]);
                }

                player.transform.position = new Vector3(172f, 0.3f, 126f);

                break;
            case 1:
                break;
            default:
                break;
        }
    }

    void Load0()
    {
        

        for (int i = 0; i < Remove0.Length; i++)
        {
            //Destroy(Remove0[i]);
            Remove0[i].SetActive(false);
        }

        for (int i = 0; i < Spawn0.Length; i++)
        {
            Spawn0[i].SetActive(true);
        }

        objektif.sudahMengambilPeta = true;
        tas.a = 10;
        tas.b = 10;
    }
}
