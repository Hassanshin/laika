﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TangramShow : MonoBehaviour {

    [Range(0,6)]
    public int urutanKe = 0;
    public int x = 1;

    public bool[] tangramAda;
    public GameObject[] tangramHint;
    public GameObject[] tangramObject;

    public GameObject Barrier;

    private void Update()
    {
        for (int i = 0; i < tangramObject.Length; i++)
        {
            tangramObject[i].SetActive(tangramAda[i]);
        }

        
        HintShow();
    }

    void nambahUrutan()
    {
        for (int i = 0; i < tangramObject.Length; i++)
        {
            if (tangramAda[i])
            {
                urutanKe = i + 1;
            }
        }
    }

    void HintShow()
    {
        if (urutanKe != x)
        {
            for (int c = 0; c < tangramHint.Length; c++)
            {
                tangramHint[c].SetActive(!true);
            }

            if(urutanKe < 7)
            {
                tangramHint[urutanKe].SetActive(true);
            } else
            {
                Barrier.SetActive(false);
            }
            

            x = urutanKe;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == ("Player"))
        {
            other.GetComponent<PlayerControl>().diTempatTangram = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == ("Player"))
        {
            other.GetComponent<PlayerControl>().diTempatTangram = !true;
        }
    }
}
