﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class KanguruScript : MonoBehaviour {

    public float speed;
    
    //public 

    Transform respawn;
    Vector3 tempatAwal;
    Animator anim;
    NavMeshAgent agent;

    public enum State
    {
        PATROL, CHASE, IDLE, SERANG, PULANG
    }

    public State state;
    private bool alive;

    

    [Header("Chasing")]
    public float chaseSpeed = 1f;
    public GameObject target;

    [Header("Serang")]
    
    public GameObject lemparObject;
    [Range(0, 2)]
    public float jedaSerang = 0.3f;

    public float jarakAntarTarget;
    public float x = 0;
    public GameObject target_Serang;

    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();

        //agent.updatePosition = true;
        //agent.updatePosition = false;

        state = KanguruScript.State.IDLE;

        alive = true;

        Awalan();
        //Invoke("awalan", 2f);

        anim.SetBool("idle", true);
        StartCoroutine("FSM");
    }

    void Awalan()
    {
        tempatAwal = transform.position;
    }

    IEnumerator FSM()
    {
        while (alive)
        {
            //PATROL, CHASE, IDLE, KAGET, PULANG
            switch (state)
            {
                case State.PATROL:
                    Patrol();
                    break;
                case State.CHASE:
                    Chase();
                    break;
                case State.IDLE:
                    Idle();
                    break;
                case State.SERANG:
                    Serang();
                    break;
                case State.PULANG:
                    Pulang();
                    break;
                default:
                    Idle();
                    break;
            }
            yield return null;
        }
    }

    void Patrol()
    {
        //Debug.Log("FSM Patrol");
    }


    void Chase()
    {
        if (target != null && target_Serang == null && !target.GetComponent<PlayerControl>().disemak)
        {
            //Debug.Log("FSM Chase");
            //if(jarakAntarTarget > 2)
            agent.SetDestination(target.transform.position);

        }
        else if (target_Serang != null)
        {
            state = KanguruScript.State.SERANG;
        }
        else
        {
            state = KanguruScript.State.IDLE;
        }

        anim.SetBool("serang", !true);
        anim.SetBool("idle", !true);
    }

    void Idle()
    {
        if (target != null)
        {
            state = KanguruScript.State.CHASE;
        }
        else if (target_Serang != null)
        {
            state = KanguruScript.State.SERANG;
        }
        else if (Vector3.Distance(transform.position, tempatAwal) > 1)
        {
            state = KanguruScript.State.PULANG;
        }

        anim.SetBool("serang", !true);
        anim.SetBool("idle", true);
    }

    void Serang()
    {

        //float xLimit = 1.0f;
        //Debug.Log("FSM Serang");

        if (target_Serang != null)
        {
            agent.velocity = Vector3.zero;
            transform.LookAt(target_Serang.transform);

            
            if (x < jedaSerang)
            {
                x += Time.deltaTime;
            } else 
            {
                //Instantiate(lemparObject, transform.position + new Vector3(0, 1, 0.2f), transform.rotation);
                x = 0;
            }
        }
        else
        {
            state = KanguruScript.State.IDLE;
            
            x = 0;
        }

        //anim.SetBool("idle", !true);
        anim.SetBool("serang", true);
    }

    void Pulang()
    {
        //Debug.Log("FSM Pulang");
        if (Vector3.Distance(transform.position, tempatAwal) < 1)
        {
            state = KanguruScript.State.IDLE;
        }
        else

        if (target != null)
        {
            state = KanguruScript.State.CHASE;
        }

        else
        {
            agent.SetDestination(tempatAwal);
        }

        anim.SetBool("idle", !true);
    }

    void Update()
    {
        if(target!= null)
        {
            jarakAntarTarget = Vector2.Distance(transform.position, target.transform.position) ;

            if (jarakAntarTarget <= 3)
            {
                target_Serang = target;
                
            } else
            {
                target_Serang = null;
            }
        } else
        {
            target_Serang = null;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other != null)
        {
            if (other.tag == "Player")
            {
                PlayerControl playerscript = other.GetComponent<PlayerControl>();
                ObjectiveChar playerobj = other.GetComponent<ObjectiveChar>();



                if (playerscript.disemak )
                {
                    target = null;
                } else
                {
                    target = other.gameObject;
                }
                //else
                //if (playerscript.currentSpeed > speedMin && !playerscript.disemak)
                //{
                //    target = other.gameObject;
                //}

            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            target = null;
            target_Serang = null;
        }
    }
}
